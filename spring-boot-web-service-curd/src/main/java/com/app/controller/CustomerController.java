package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.app.entities.Customer;
import com.app.repository.CustomerRepository;

@RestController
public class CustomerController {

	@Autowired
	private CustomerRepository customerRepository;

//	@GetMapping(value = { "customer/{id}" })
//	public ResponseEntity<Customer> byId(@PathVariable(value = "id") Integer id) {
//		Customer customer = customerRepository.findById(id).get();
//		return ResponseEntity.ok().body(customer);
//	}

	@GetMapping(value = "customers", produces= {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<Customer>> customers() {
		List<Customer> customers = customerRepository.findAll();
		return ResponseEntity.ok().body(customers);
	}

	@PostMapping(value = { "customer/save" })
	public ResponseEntity<Customer> saveOrUpdate(@RequestBody Customer customer) {
		Customer newCustomer = customerRepository.save(customer);
		return ResponseEntity.ok().body(newCustomer);
	}

//	@DeleteMapping(value = { "customer/delete/{id}" })
//	public ResponseEntity<String> deleteCustomer(@PathVariable("id") Integer id) {
//		Boolean flag = Boolean.FALSE;
//		customerRepository.deleteById(id);
//		flag = Boolean.TRUE;
//		if (flag)
//			return ResponseEntity.ok().body("Delete Recorded with id : " + id);
//		else
//			return ResponseEntity.ok().body("Record not deleted with id : " + id);
//	}

	@GetMapping(value = "/{name}")
	public ResponseEntity<Customer> findCustomerByName(@PathVariable("name") String name) {
		Customer customer = customerRepository.findByName(name);
		return ResponseEntity.ok().body(customer);
	}

}
