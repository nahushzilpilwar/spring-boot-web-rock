package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootWebServiceCurdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebServiceCurdApplication.class, args);
	}

}
