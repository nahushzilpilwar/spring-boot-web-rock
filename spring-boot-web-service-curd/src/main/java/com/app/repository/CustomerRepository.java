package com.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.entities.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

	public Customer findByName(String name);

	@Query("from Customer c where c.mobile=:mob")
	public Customer abc(String mob);

}
